@echo off
SETLOCAL EnableDelayedExpansion

rem This script creates:-
rem 	- One database 
rem	- Ralated tables in the database and pre-populate them with default values
rem	- Stored procedures
rem	- Triggers
rem	- Views
rem
rem Authored by:-
rem Name:  caren misik
rem Email: carenmisik@gmail.com
rem Date:  12th dec 2018

rem shopt -s -o nounset

set databaseName="MEEEM"
set lclhost="localhost"
set /p username="Enter MySql UserName:" %=%

mysql_config_editor set --login-path=local --host=%lclhost% --user=%username% --password

echo.... Script Started...

rem List databases that are there in the SQL instance
call:ExecuteSqlCommand "show databases"

rem Deletes the database if it already exists
echo.Deleting $databaseName if it exists
call:ExecuteSqlCommand "DROP DATABASE IF EXISTS MEEEM"

rem Create the database
echo.Creating databaseName database
call:ExecuteSqlCommand "CREATE DATABASE IF NOT EXISTS MEEEM"

rem List databases that are there in the SQL instance after create
call:ExecuteSqlCommand "show databases"


rem 	Create USER table and the related structures
echo.Creating user ...
call:ExecuteSqlTableCommand "CREATE TABLE IF NOT EXISTS USER (MEEEM_user_username varchar(20) NOT NULL, MEEEM_user_passwd INT NOT NULL, PRIMARY KEY (MEEEM_user_username))  ENGINE=INNODB;"


rem 	Create branches table and the related structures
echo.Creating branchesTable ...
call:ExecuteSqlTableCommand "CREATE TABLE IF NOT EXISTS branches (MEEEM_branches_pastorsname varchar(50) NOT NULL, MEEEM_branches_county VARCHAR(20)  NULL, MEEEM_brances_province varchar(50) NOT NULL, MEEEM_branches_pastorincharge varchar(50) NOT NULL, MEEEM_branches_branchesid varchar(10) NOT NULL, PRIMARY KEY (MEEEM_branches_branchesid))  ENGINE=INNODB;"


rem 	Create EVANGELISM table and the related structures
echo.Creating evangelismTable ...
call:ExecuteSqlTableCommand "CREATE TABLE IF NOT EXISTS EVANGELISM (MEEEM_evangelism_date date NOT NULL , MEEEM_evangelism_venueid varchar(20) NOT NULL, MEEEM_evangelism_venue varchar(20) NOT NULL, MEEEM_evangelism_noofdays int NOT NULL,PRIMARY KEY (MEEEM_evangelism_venueid))  ENGINE=INNODB;"


rem ####################

echo.&pause&goto:eof

:ExecuteSqlCommand
	mysql --login-path=local -e"%~1"
goto:eof

:ExecuteSqlTableCommand
	mysql --login-path=local -D"%databaseName%" -e"%~1"
goto:eof
