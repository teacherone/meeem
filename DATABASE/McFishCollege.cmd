@echo off
SETLOCAL EnableDelayedExpansion

rem This script creates:-
rem 	- One database 
rem	- Ralated tables in the database and pre-populate them with default values
rem	- Stored procedures
rem	- Triggers
rem	- Views
rem
rem Authored by:-
rem Name:  Felix Otieno Okoth
rem Email: felix@mansoftonline.com
rem Date:  Jul 28th 2014

rem shopt -s -o nounset

set databaseName="YOUR_DATABASE_NAME"
set lclhost="localhost"
set /p username="Enter MySql UserName:" %=%

mysql_config_editor set --login-path=local --host=%lclhost% --user=%username% --password

echo.... Script Started...

rem List databases that are there in the SQL instance
call:ExecuteSqlCommand "show databases"

rem Deletes the database if it already exists
echo.Deleting $databaseName if it exists
call:ExecuteSqlCommand "DROP DATABASE IF EXISTS YOUR_DATABASE_NAME"

rem Create the database
echo.Creating databaseName database
call:ExecuteSqlCommand "CREATE DATABASE IF NOT EXISTS YOUR_DATABASE_NAME"

rem List databases that are there in the SQL instance after create
call:ExecuteSqlCommand "show databases"

rem 	Create LanguageTable table and the related structures
echo.Creating LanguageTable ...
call:ExecuteSqlTableCommand "CREATE TABLE IF NOT EXISTS LanguageTable (mcfish_language_lciddec INT NOT NULL, mcfish_language_name VARCHAR(100) NOT NULL, mcfish_language_lcidhex VARCHAR(5) NOT NULL, mcfish_language_lastmodfd DATETIME NOT NULL DEFAULT '2000-01-03 04:30:43', PRIMARY KEY (mcfish_language_lciddec)) ENGINE=INNODB;"

rem	Create PopulateLanguageTable function 
echo.Creating PopulateLanguageTable function ...
call:ExecuteSqlTableCommand "INSERT INTO LanguageTable (mcfish_language_lciddec, mcfish_language_name, mcfish_language_lcidhex) VALUES (1101, 'Assamese', '044d'),(1093, 'Bengali (India)', '0445'),(1109, 'Burmese', '0455'),(1126, 'Edo', '0466'),(16393, 'English (India)', '4009'),(1124, 'Filipino', '0464'),	(11276, 'French (Cameroon)', '02c0c'),(9228, 'French (Congo, DRC)', '240c'),(12300, 'French (Cote d\'Ivoire)', '300c'),(13324, 'French (Mali)', '340c'),(14348, 'French (Morocco)', '380c'),(10252, 'French (Senegal)', '280c'),(7180, 'French (West Indies)', '1c0c'),(1122, 'Frisian (Netherlands)', '0462'),(2108, 'Gaelic Ireland', '083c'),(1084, 'Gaelic Scotland', '043c'),(1140, 'Guarani (Paraguay)', '0474'),(1279, 'HID (Human Interface Device)', '04ff'),(1136, 'Igbo (Nigeria)', '0470'),(1120, 'Kashmiri', '0460'),(1107, 'Khmer', '0453'),(1108, 'Lao', '0454'),(1142, 'Latin', '0476'),(1100, 'Malayalam', '044c'),(1082, 'Maltese', '043a'),(1112, 'Manipuri', '0458'),(1153, 'Maori (New Zealand)', '0481'),(1121, 'Nepali', '0461'),(1096, 'Oriya', '0448'),(1047, 'Rhaeto-Romanic', '0417'),(2072, 'Romanian (Moldova)', '0818'),(2073, 'Russian (Moldova)', '0819'),(1083, 'Sami Lappish', '043b'),(1072, 'Sesotho', '0430'),(1113, 'Sindhi', '0459'),(1115, 'Sinhalese (Sri Lanka)', '045b'),(1143, 'Somali', '0477'),(1070, 'Sorbian', '042a'),(1064, 'Tajik', '0428'),(1105, 'Tibetan', '0451'),(1073, 'Tsonga', '0431'),(1074, 'Tswana', '0432'),(1090, 'Turkmen', '0442'),(1075, 'Venda', '0433'),(1106, 'Welsh', '0452'),(1076, 'Xhosa', '0434'),(1085, 'Yiddish', '043d'),(1077, 'Zulu', '0435'),(1052, 'Albanian', '041c'),(14337, 'Arabic (U.A.E.)', '3801'),(11274, 'Spanish (Argentina)', '2c0a'),(1067, 'Armenian', '042b'),(3081, 'English (Australia)', '0c09'),(3079, 'German (Austria)', '0c07'),(2092, 'Azeri (Cyrillic)', '082c'),(1068, 'Azeri (Latin)', '042c'),(2067, 'Dutch (Belgium)', '0813'),(2060, 'French (Belgium)', '080c'),(2117, 'Bengali (Bangladesh)', '0845'),(1026, 'Bulgarian', '0402'),(15361, 'Arabic (Bahrain)', '3c01'),(5146, 'Bosnian (Bosnia/Herzegovina)', '141A'),(4122, 'Croatian (Bosnia/Herzegovina)', '101a'),(1059, 'Belarusian', '0423'),(10249, 'English (Belize)', '2809'),(16394, 'Spanish (Bolivia)', '400a'),(1046, 'Portuguese (Brazil)', '0416'),(2110, 'Malay (Brunei Darussalam)', '083e'),(4105, 'English (Canada)', '1009'),(3084, 'French (Canada)', '0c0c'),(9225, 'English (Caribbean)', '2409'),(4108, 'French (Switzerland)', '100c'),(2055, 'German (Switzerland)', '0807'),	(2064, 'Italian (Switzerland)', '0810'),(13322, 'Spanish (Chile)', '340a'),(2052, 'Chinese (PRC)', '0804'),(9226, 'Spanish (Colombia)', '240a'),(5130, 'Spanish (Costa Rica)', '140a'),(1029, 'Czech', '0405'),(1031, 'German (Germany)', '0407'),(1030, 'Danish', '0406'),(7178, 'Spanish (Dominican Republic)', '1c0a'),(5121, 'Arabic (Algeria)', '1401'),(12298, 'Spanish (Ecuador)', '300a'),(3073, 'Arabic (Egypt)', '0c01'),(1069, 'Basque', '042d'),(1027, 'Catalan', '0403'),(1110, 'Galician', '0456'),(3082, 'Spanish (International Sort)', '0c0a'),(1034, 'Spanish (Traditional Sort)', '040a'),(1061, 'Estonian', '0425'),(1118, 'Amharic (Ethiopia)', '045e'),(1035, 'Finnish', '040b'),(2077, 'Swedish (Finland)', '081d'),(1036, 'French (France)', '040c'),(1080, 'Faroese', '0438'),(2057, 'English (United Kingdom)', '0809'),(1079, 'Georgian', '0437'),(1032, 'Greek', '0408'),(4106, 'Spanish (Guatemala)', '100a'),(3076, 'Chinese (Hong Kong S.A.R.)', '0c04'),(18442, 'Spanish (Honduras)', '480a'),(1050, 'Croatian', '041a'),(1038, 'Hungarian', '040e'),(1057, 'Indonesian', '0421'),(1095, 'Gujarati', '0447'),(1081, 'Hindi', '0439'),(1099, 'Kannada', '044b'),(1111, 'Konkani', '0457'),(1102, 'Marathi', '044e'),(1094, 'Punjabi', '0446'),(1103, 'Sanskrit', '044f'),(1097, 'Tamil', '0449'),(1098, 'Telugu', '044a'),(6153, 'English (Ireland)', '1809'),(1065, 'Farsi', '0429'),(2049, 'Arabic (Iraq)', '0801'),(1039, 'Icelandic', '040f'),(1037, 'Hebrew', '040d'),(1040, 'Italian (Italy)', '0410'),(8201, 'English (Jamaica)', '2009'),(11265, 'Arabic (Jordan)', '2c01'),(1041, 'Japanese', '0411'),(1087, 'Kazakh', '043f'),(1089, 'Swahili', '0441'),(1088, 'Kyrgyz (Cyrillic)', '0440'),(1042, 'Korean', '0412'),(13313, 'Arabic (Kuwait)', '3401'),(12289, 'Arabic (Lebanon)', '3001'),(4097, 'Arabic (Libya)', '1001'),(5127, 'German (Liechtenstein)', '1407'),(1063, 'Lithuanian', '0427'),(5132, 'French (Luxembourg)', '140c'),(4103, 'German (Luxembourg)', '1007'),(1062, 'Latvian', '0426'),(6145, 'Arabic (Morocco)', '1801'),(5124, 'Chinese (Macau S.A.R.)', '1404'),(6156, 'French (Monaco)', '180c'),(1125, 'Divehi', '0465'),(2058, 'Spanish (Mexico)', '080a'),(1071, 'FYRO Macedonian', '042f'),(1104, 'Mongolian (Cyrillic)', '0450'),(2128, 'Mongolian (Mongolia)', '0850'),(1086, 'Malay (Malaysia)', '043e'),(19466, 'Spanish (Nicaragua)', '4c0a'),(1043, 'Dutch (Netherlands)', '0413'),(1044, 'Norwegian (Bokmal)', '0414'),(2068, 'Norwegian (Nynorsk)', '0814'),(5129, 'English (New Zealand)', '1409'),(8193, 'Arabic (Oman)', '2001'),(1056, 'Urdu', '0420'),(6154, 'Spanish (Panama)', '180a'),(10250, 'Spanish (Peru)', '280a'),(13321, 'English (Philippines)', '3409'),(1045, 'Polish', '0415'),(20490, 'Spanish (Puerto Rico)', '500a'),(2070, 'Portuguese (Portugal)', '0816'),(15370, 'Spanish (Paraguay)', '3c0a'),(16385, 'Arabic (Qatar)', '4001'),(1048, 'Romanian', '0418'),(1049, 'Russian', '0419'),(1092, 'Tatar', '0444'),(1025, 'Arabic (Saudi Arabia)', '0401'),(4100, 'Chinese (Singapore)', '1004'),(17418, 'Spanish (El Salvador)', '440a'),(3098, 'Serbian (Cyrillic)', '0c1a'),(2074, 'Serbian (Latin)', '081a'),(1051, 'Slovak', '041b'),(1060, 'Slovenian', '0424'),(1053, 'Swedish', '041d'),(10241, 'Arabic (Syria)', '2801'),(1114, 'Syriac', '045a'),(1054, 'Thai', '041e'),(11273, 'English (Trinidad)', '2c09'),(7169, 'Arabic (Tunisia)', '1c01'),(1055, 'Turkish', '041f'),(1028, 'Chinese (Taiwan)', '0404'),(1058, 'Ukrainian', '0422'),(14346, 'Spanish (Uruguay)', '380a'),(1033, 'English (United States)', '0409'),(2115, 'Uzbek (Cyrillic)', '0843'),(1091, 'Uzbek (Latin)', '0443'),(8202, 'Spanish (Venezuela)', '200a'),(1066, 'Vietnamese', '042a'),(9217, 'Arabic (Yemen)', '2401'),(1078, 'Afrikaans', '0436'),(7177, 'English (South Africa)', '1c09'),(12297, 'English (Zimbabwe)', '3009');"


echo.Creating SchoolLvlSubStg ...
call:ExecuteSqlTableCommand "CREATE TABLE IF NOT EXISTS SchoolLvlSubStg (mcfish_schlvlstg_id INT NOT NULL AUTO_INCREMENT, mcfish_schlvlstg_grp ENUM('COMPULSORY', 'SCIENCE', 'HUMANITY', 'TECHNICALANDAPPLIED', 'OTHERS') NOT NULL, mcfish_schlvlstg_no INT NOT NULL, mcfish_schlvlstg_lvl INT NOT NULL, mcfish_schlvlstg_tenant CHAR(36) NOT NULL, PRIMARY KEY (mcfish_schlvlstg_id), UNIQUE (mcfish_schlvlstg_tenant, mcfish_schlvlstg_lvl, mcfish_schlvlstg_grp), FOREIGN KEY (mcfish_schlvlstg_tenant) REFERENCES TenantTable(mcfish_tenant_uuid) ON UPDATE CASCADE ON DELETE RESTRICT, FOREIGN KEY (mcfish_schlvlstg_lvl) REFERENCES SchoolLevel(mcfish_schlvl_id) ON UPDATE CASCADE ON DELETE RESTRICT) ENGINE=INNODB;"


rem	Alter SchoolStudSubject
	echo.Alter SchoolStudSubject ...
	call:ExecuteSqlTableCommand "ALTER TABLE SchoolStudSubject ADD mcfish_schstusub_pts INT;"

rem	DROP InventoryDelivery
	echo.Drop InventoryDelivery ...
	call:ExecuteSqlTableCommand "DROP TABLE IF EXISTS InventoryDelivery;"

rem ####################

echo.&pause&goto:eof

:ExecuteSqlCommand
	mysql --login-path=local -e"%~1"
goto:eof

:ExecuteSqlTableCommand
	mysql --login-path=local -D"%databaseName%" -e"%~1"
goto:eof
