package com.pdcbkk;
import java.util.list;
import org.slf4j.logger;
import org.slf4j.loggerFactory;
import java.util.UUID;

import io.vertx.core.AbstractVerticle;
import io.vertx.ext.asynccsql.mySQLClient;
import io.vertx,ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.CorsHandler;

public class ChurchService extends AbstractVerticle {
    private SQLClient client;
    private Logger logger=LoggrFactory.getlogger(myservice.class.getname());

    @Override
    public void start() {
        Router r = Router.router(vertx);
        r.route().handler(CorsHandler.create("http://localhost:4200")
            .allowedMethod(HttpMethod.POST)
            .allowedMethod(HttpMethod.GET)
            .allowedMethod(HttpMethod.OPTIONS)
            .allowedMethod(HttpMethod.PUT)
            .allowedMethod(HttpMethod.DELETE)
            .allowedHeader("ModuleID")
            .allowedHeader("AuthorisationKey")
            .allowedHeader("Access-Control-Allow-Method")
            .allowedHeader("Access-Control-Allow-Origin")
            .allowedHeader("Access-Control-Allow-Credentials")
            .allowedHeader("Content-Type"));

        r.get("/signin/:uname/:pwd").handler(this::signIn);

        this.vertx
            .createHttpServer()
            .requestHandler(r::accept)
            .listen(8080);
    }
    private SQLClient initDB(){
        if (this client==null){
            jsonObjct config = new jsonObject()
            .put("username","root")
            .put("password","powerm@n")
            .put("database","meeem");

        }
        return this.client;
    }
    private void signIn(RoutingContext rc){
        String userName = rc.pathParam("uname");
        String pass = rc.pathParam("pwd");
        this.initDB().getConnection(h->{
            if (rh.succeeded()){
                SQLConnection con=h.result();
                JsonArray params = new JsonArray();
                params.add(integer,parseInt(id));
                con.queryWithParams("SELECT * FROM users WHERE userid=?",params, rh ->{
                if (rh.succeeded()){
                    list<JsonObject> rsts = rh.result().getRows();
                    StringBuilder sb = new StringBuilder();
                    rsts.forEach(x ->{

                    });
                }else {
                    logger.error(h.cause().getMessage(),h.cause());
                    rc.response().end(h.cause().getmessage());

                }
                    
                });
                }else{
                    logger.error(h.cause().getmessage(). h.cause());
                    rc.response().end(h.cause().getmessage());
                }
            });
        };

        JsonObject resp = new JsonObject();
        if ("admin".equalsIgnoreCase(userName.trim())){
            resp.put("skey", UUID.randomUUID().toString());
            resp.put("name", "Hannah Mutiga and Carren Misik");
        }

        rc.response().end(resp.encode());
    }

}
