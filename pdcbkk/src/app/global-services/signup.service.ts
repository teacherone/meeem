import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Register } from '../classes/register';

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  constructor(private http: HttpClient) { }

  signupUser(u: Register){
    return this.http.get('http://localhost:8080/signin/' + u.surname + '/' + u.password);
  }
}
