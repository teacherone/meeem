import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core'

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SignupComponent } from './signup/signup.component';
import { FooterComponent } from './footer/footer.component';
import { BodyComponent } from './body/body.component';
import { BaseComponent } from './base/base.component';
import { EventsComponent } from './events/events.component';
import { BranchesComponent } from './branches/branches.component';
import { ServicesComponent } from './services/services.component';
import { from } from 'rxjs';
import { ContactsComponent } from './contacts/contacts.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SignupComponent,
    FooterComponent,
    BodyComponent,
    BaseComponent,
    EventsComponent,
    BranchesComponent,
    ServicesComponent,
    ContactsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
