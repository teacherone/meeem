import { NgModule } from '@angular/core';
import{} from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { BaseComponent } from './base/base.component';
import { EventsComponent } from './events/events.component';
import { BranchesComponent } from './branches/branches.component';
import { ServicesComponent } from './services/services.component';
import {ContactsComponent} from './contacts/contacts.component';
import { importType } from '@angular/compiler/src/output/output_ast';

const routes: Routes = [
  { path: 'home', component: BaseComponent },
  { path: 'events', component: EventsComponent },
  { path: 'branches', component: BranchesComponent },
  { path: 'services', component: ServicesComponent },
  { path: 'Contacts', component: ContactsComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {enableTracing: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
