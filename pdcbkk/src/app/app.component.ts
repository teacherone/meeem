import { Component } from '@angular/core';
import { Register } from './classes/register';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pdcbkk';
  
  reg:Register={
    surname: undefined,
    othernames: undefined,
    Email:undefined,
    password:undefined
  };
}
