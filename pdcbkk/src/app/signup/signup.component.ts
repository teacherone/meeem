import { Component, OnInit } from '@angular/core';
import { Register } from '../classes/register';
import { SignupService } from '../global-services/signup.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  reg: Register = {
    Email:undefined,
    othernames:undefined,
    password:undefined,
    surname:undefined
  };

  constructor(
    //public Email: string,
   // public othernames: string,
   // public password: string,
    //public surname: string,
    private signupService: SignupService) {}

  ngOnInit() {
  }

  signup(r: Register) : void {
        this.signupService.signupUser(r).subscribe((data: any) => {
          alert(JSON.stringify(data));
        });
  }

}
